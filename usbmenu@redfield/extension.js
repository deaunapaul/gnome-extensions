// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Lang = imports.lang;
const St = imports.gi.St;
const Main = imports.ui.main;
const Gtk = imports.gi.Gtk;
const Gio = imports.gi.Gio;
const Signals = imports.signals;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const GLib = imports.gi.GLib;
const Gettext = imports.gettext.domain('gnome-shell-extensions');
const _ = Gettext.gettext;

const Me = imports.misc.extensionUtils.getCurrentExtension();
const DBusIface = Me.imports.dbus;
const dialog = Me.imports.dialog;

let usbSubMenu = null;
var dbus_server = null;

var AggregateMenuIndicator = new Lang.Class({
    Name: 'AggregateMenuIndicator',
    Extends: PanelMenu.SystemIndicator,

    _init: function() {
        this.parent();
        this._primaryIndicator = this._addIndicator();
        this._primaryIndicator.icon_name = "usb-symbolic";
        this._primaryIndicator.style_class = "system-status-icon no-padding";
        this.indicators.show();
    },
});

const UsbSubMenu = new Lang.Class({
    Name: 'UsbSubMenu',
    Extends: PopupMenu.PopupSubMenuMenuItem,

    _init: function() {
        this.parent('', true);

        this.icon.style_class = 'icon-label';
        this.icon.icon_name = 'usb-symbolic';

        this.label.set_text("USB Controls");

        this.assignItem = new PopupMenu.PopupMenuItem('Assign USB Device');
        this.assignItem.connect('activate', Lang.bind(this, this._showAssignDialog));
        this.menu.addMenuItem(this.assignItem);

        this.removeItem = new PopupMenu.PopupMenuItem('Remove USB Device');
        this.removeItem.connect('activate', Lang.bind(this, this._showRemoveDialog));
        this.menu.addMenuItem(this.removeItem);

        this.usbDeviceList = [ ];
        this.vmList = [ ];

        this.indicator = new AggregateMenuIndicator();
    },
    _showAssignDialog: function() {
        this._dialog = new dialog.UsbAssignDialog();
        this._dialog.connect('closed', this._dialogClosed.bind(this));

        var usbDeviceList = dbus_server.DeviceList()["usb_devices"];

        for (let usbDevice in usbDeviceList) {
            if (!usbDeviceList[usbDevice].hasOwnProperty('name')) {
                continue;
            }

            if (usbDeviceList[usbDevice]["state"] == 1) {
                continue;
            }

            this._dialog._usbDeviceAdded(usbDeviceList[usbDevice]);
        }

        this._dialog.open();
    },
    _showRemoveDialog: function() {
        this._dialog = new dialog.UsbRemoveDialog();
        this._dialog.connect('closed', this._dialogClosed.bind(this));

        var usbDeviceList = dbus_server.DeviceList()["usb_devices"];

        for (let usbDevice in usbDeviceList) {
            if (!usbDeviceList[usbDevice].hasOwnProperty('name')) {
                continue;
            }

            if (usbDeviceList[usbDevice]["state"] == 0) {
                continue;
            }

            this._dialog._usbDeviceAdded(usbDeviceList[usbDevice]);
        }

        this._dialog.open();
    },
    _dialogClosed() {
        this._dialog = null;
    },
    destroy: function() {
        this.parent();
    }
});

function init() {
}

function enable() {
    usbSubMenu = new UsbSubMenu();

    // Try to add the output-switcher right below the output slider...
    let aggregateMenuPanelButton = Main.panel.statusArea['aggregateMenu'];
    let powerIndicator = aggregateMenuPanelButton._power;
    let desiredMenuPosition = aggregateMenuPanelButton.menu._getMenuItems().indexOf(Main.panel.statusArea.aggregateMenu._rfkill.menu);
    let powerSubmenuPosition = aggregateMenuPanelButton.menu._getMenuItems().indexOf(powerIndicator.menu);
    aggregateMenuPanelButton._indicators.insert_child_below(usbSubMenu.indicator.indicators, powerIndicator.indicators);

    let menu = Main.panel.statusArea.aggregateMenu.menu;
    menu.addMenuItem(usbSubMenu, desiredMenuPosition);

    dbus_server = new DBusIface.usbDbusServer(usbSubMenu);

    global.log('USB shell extension started');
}

function disable() {
    global.log('USB shell extension terminating');
    dbus_server.disable();
    usbSubMenu.indicator.indicators.destroy();
    usbSubMenu.indicator = null;
    usbSubMenu.destroy();
    usbSubMenu = null;
}
