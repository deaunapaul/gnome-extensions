// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

const Lang = imports.lang;
const St = imports.gi.St;
const Main = imports.ui.main;
const Gtk = imports.gi.Gtk;
const Gio = imports.gi.Gio;
const Signals = imports.signals;
const Tweener = imports.ui.tweener;
const Util = imports.misc.util;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;
const GLib = imports.gi.GLib;
const Gettext = imports.gettext.domain('gnome-shell-extensions');
const _ = Gettext.gettext;

const Me = imports.misc.extensionUtils.getCurrentExtension();
const DBusIface = Me.imports.dbus;
const dialog = Me.imports.dialog;

let wifiSubMenu = null;
var netctl_dbus = null;

var wireless_symbols = {
    'offline':'network-wireless-offline-symbolic',
    'connected':'network-wireless-connected-symbolic',
    'acquiring':'network-wireless-acquiring-symbolic',
    'encrypted':'network-wireless-encrypted-symbolic',
    'none':'network-wireless-signal-none-symbolic',
    'Very Weak':'network-wireless-signal-weak-symbolic',
    'Weak':'network-wireless-signal-ok-symbolic',
    'Fair':'network-wireless-signal-good-symbolic',
    'Very Good':'network-wireless-signal-excellent-symbolic',
    'Excellent':'network-wireless-signal-excellent-symbolic',
    'Unknown':'network-error-symbolic',
    'error':'network-error-symbolic'
};

var AggregateMenuIndicator = new Lang.Class({
    Name: 'AggregateMenuIndicator',
    Extends: PanelMenu.SystemIndicator,

    _init: function() {
        this.parent();
        this._primaryIndicator = this._addIndicator();
        this._primaryIndicator.icon_name = "network-wireless-offline-symbolic";
        this._primaryIndicator.style_class = "system-status-icon no-padding";
        this.indicators.show();      
    },
});

const WifiSubMenu = new Lang.Class({
    Name: 'WifiSubMenu',
    Extends: PopupMenu.PopupSubMenuMenuItem,

    _init: function() {
        this.parent('', true);
        this.active_ssid = null;

        this.icon.style_class = 'icon-label';
        this.icon.icon_name = 'network-wireless-offline-symbolic';

        this.label.set_text("Wi-Fi Off");

        this.selectItem = new PopupMenu.PopupMenuItem('Select Network');
        this.selectItem.connect('activate', Lang.bind(this, this._selectNetwork));
        this.menu.addMenuItem(this.selectItem);

        this.disconnectItem = new PopupMenu.PopupMenuItem("Disconnect");
        this.disconnectItem.connect('activate', Lang.bind(this, this._disconnectNetwork));
        this.menu.addMenuItem(this.disconnectItem);

        /*
        // These menu entries may be used at a later date.
        this.toggleItem = new PopupMenu.PopupSwitchMenuItem("Turn Off");
        this.toggleItem.connect('activate', Lang.bind(this, this._toggleNetwork));
        this.menu.addMenuItem(this.toggleItem);

        this.settingsItem = new PopupMenu.PopupMenuItem("Network Settings");
        this.settingsItem.connect('activate', Lang.bind(this, this._networkSettings));
        this.menu.addMenuItem(this.settingsItem);
        */

        this.netlist = [ ];

        this.indicator = new AggregateMenuIndicator();
    },

    _showDialog: function() {
        this._dialog = new dialog.WifiSelectDialog(0, 0, this.active_ssid);
        this._dialog.connect('closed', this._dialogClosed.bind(this));


        for (let network of this.netlist) {
            if (!network.hasOwnProperty('ssid')) {
                continue;
            }
            var ap = new dialog.AccessPoint(network.ssid, network.key_mgmt, network.signal_strength)
            var apsec = dialog.apsecurity[network.key_mgmt];
            var net = {
                ssid: network.ssid,
                key_mgmt: network.key_mgmt,
                security: apsec,
                connections: [ ],
                item: null,
                accessPoints: [ ap ],
                signal_strength: network.signal_strength
            };
            this._dialog._accessPointAdded(null, net);
            
        }
        // ssid, signal_strength, key_mgmt: wpa-psk
        this._dialog.open();
    },

    _dialogClosed() {
        //this._dialog.destroy();
        this._dialog = null;
    },

    _populateNetworkSelect: function(ssidlist) {

    },

    connectedTo: function(ssid) {
        this.active_ssid = ssid;
        this.updateNetworkName(ssid);
    },

    disconnected: function () {
        this.active_ssid = null;
        this.updateNetworkName("Wi-Fi Not connected");
        this.updateIcon("offline");
    },
    
    updateIcon: function(signal) {
        if (wireless_symbols.hasOwnProperty(signal)) {
            this.icon.icon_name = wireless_symbols[signal];
            this.indicator._primaryIndicator.icon_name = wireless_symbols[signal]
        }
    },
    updateIP: function(ip) {
	if (ip == "") {
	    if (this.ipAddress) {
		this.ipAddress.destroy();
		this.ipAddress = null;
	    }
	}
	else {
	    if (this.ipAddress != null) {
		this.ipAddress.destroy();
	    }
	    this.ipAddress = new PopupMenu.PopupMenuItem("IP: " + ip);
	    this.menu.addMenuItem(this.ipAddress);
	}

    },
    updateNetworkName: function(name) {
        this.label.set_text(name);
    },
    
    _selectNetwork: function() {
        this._showDialog();
    },
    
    _toggleNetwork: function() {

    },
    connect_to_network: function(net, secret) {
        var obj = {
            client: { uuid: this.vmuuid },
            type: DBusIface.pbtypes.cmdConnect,
            iface: {
                name: this.iface
            },
            wifi_conf: {
                Ssid: net.ssid,
                Psk: secret.psk,
                KeyMgmt: net.security,
                Identity: secret.identity,
                Password: secret.password,
                Eap: secret.eap,
                CaCert: secret.cacert,
                ClientCert: secret.clientcert,
                PrivKey: secret.privkey,
                PrivKeyPasswd: secret.privkeypass
            }
        };
        netctl_dbus.sendCommand(obj);
    },
    
    _disconnectNetwork: function() {
        var obj = {
            client: { uuid: this.vmuuid },
            type: DBusIface.pbtypes.cmdDisconnect,
            iface: {
                name: this.iface
            },
        };
        netctl_dbus.sendCommand(obj);
    },
    
    _networkSettings: function() {

    },

    destroy: function() {
        this.parent();
    }
    
});

function init() {
}

function enable() {
    wifiSubMenu = new WifiSubMenu();

    // Try to add the output-switcher right below the output slider...
    let aggregateMenuPanelButton = Main.panel.statusArea['aggregateMenu'];
    let powerIndicator = aggregateMenuPanelButton._power;
    let desiredMenuPosition = aggregateMenuPanelButton.menu._getMenuItems().indexOf(Main.panel.statusArea.aggregateMenu._rfkill.menu);
    let powerSubmenuPosition = aggregateMenuPanelButton.menu._getMenuItems().indexOf(powerIndicator.menu);
    aggregateMenuPanelButton._indicators.insert_child_below(wifiSubMenu.indicator.indicators, powerIndicator.indicators);

    let menu = Main.panel.statusArea.aggregateMenu.menu;
    menu.addMenuItem(wifiSubMenu, desiredMenuPosition);
    
    netctl_dbus = new DBusIface.netctlDBusInterface(wifiSubMenu);

    global.log('Wi-Fi shell extension started');
}

function disable() {
    global.log('Wi-Fi shell extension terminating');
    netctl_dbus.disable();
    wifiSubMenu.indicator.indicators.destroy();
    wifiSubMenu.indicator = null;
    wifiSubMenu.destroy();
    wifiSubMenu = null;
}

