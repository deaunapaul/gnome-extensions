// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// This file is largely composed of code snippets pieced together from
// gnome-shell js/ui/components/networkAgent.js and js/ui/status/network.js
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.



const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const ModalDialog = imports.ui.modalDialog;
const Dialog = imports.ui.dialog;
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const Animation = imports.ui.animation;
const Signals = imports.signals;
const Util = imports.misc.util;
const Main = imports.ui.main;
const Pango = imports.gi.Pango;
const ShellEntry = imports.ui.shellEntry;
const Shell = imports.gi.Shell;
const PopupMenu = imports.ui.popupMenu;
const Atk = imports.gi.Atk;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const DBusIface = Me.imports.dbus;
const wifimenu = Me.imports.extension;
const Spawn = Me.imports.spawn;

var apsecurity = {
    'none': 'NONE',
    'unknown': 'NONE',
    'wpa-psk': 'WPA-PSK',
    'wpa-eap': 'WPA-EAP',
    'ieee8021x': 'WPA2_ENT'
};

var AccessPoint = new Lang.Class ({
    Name: 'AccessPoint',

    _init(ssid, keymgmt, signal_strength) {
        this._ssid = ssid;
        this.ssid = new SSID(ssid);
        this.keymgmt = keymgmt;
        this.strength = signal_strength;
        this._secType = apsecurity[keymgmt];

        this.data = { ssid: this._ssid, get_data: function() {return this.ssid}};

        this.get_ssid = (() => { return this.ssid; }).bind(this);
    }
});

var SSID = new Lang.Class ({
    Name: "SSID",
    _init(ssid) {
        this.ssid = ssid;
    },
    get_data: function() {
        global.log("get_data");
        return this.ssid;
    }
});

var ActionComboBox = new Lang.Class({
    Name: 'ActionComboBox',

    _init() {
        this.actor = new St.Button({ style_class: 'modal-dialog-linked-button combo-box-label' });
        this.actor.connect('clicked', this._onButtonClicked.bind(this));
        this.actor.set_toggle_mode(true);

        let boxLayout = new Clutter.BoxLayout({ orientation: Clutter.Orientation.HORIZONTAL,
                                                spacing: 2 });
        let box = new St.Widget({ layout_manager: boxLayout });
        this.actor.set_child(box);

        this._label = new St.Label({ style_class: 'combo-box-label' });
        box.add_child(this._label)

        let arrow = new St.Icon({ style_class: 'popup-menu-arrow',
                                  icon_name: 'pan-down-symbolic',
                                  accessible_role: Atk.Role.ARROW,
                                  y_expand: true,
                                  y_align: Clutter.ActorAlign.CENTER });
        box.add_child(arrow);

        this._editMenu = new PopupMenu.PopupMenu(this.actor, 0, St.Side.TOP);
        this._editMenu.connect('menu-closed', () => {
            this.actor.set_checked(false);
        });
        this._editMenu.actor.hide();
        Main.uiGroup.add_actor(this._editMenu.actor);

        this._actionLabels = new Map();
        this._actionLabels.set(0, "");
        this._actionLabels.set(1, "TLS");
        this._actionLabels.set(2, "LEAP");
        this._actionLabels.set(3, "TTLS");

        this._buttonItems = [];

        for (let [action, label] of this._actionLabels.entries()) {
            let selectedAction = action;
            let item = this._editMenu.addAction(label, () => {
                this._onActionSelected(selectedAction);
            });

            /* These actions only apply to pad buttons */
            if (selectedAction == 1 ||
                selectedAction == 2)
                this._buttonItems.push(item);
        }

        this.setAction(0);
    },

    _onActionSelected(action) {
        this.setAction(action);
        this.popdown();
        this.emit('action-selected', action);
    },

    setAction(action) {
        this._label.set_text(this._actionLabels.get(action));
    },

    popup() {
        this._editMenu.open(true);
    },

    popdown() {
        this._editMenu.close(true);
    },

    _onButtonClicked() {
        if (this.actor.get_checked())
            this.popup();
        else
            this.popdown();
    },

    setButtonActionsActive(active) {
        this._buttonItems.forEach(item => { item.setSensitive(active); });
    }
});
Signals.addSignalMethods(ActionComboBox.prototype);


var WirelessDialogItem = new Lang.Class({
    Name: 'WirelessDialogItem',

    _init(network) {
        this._network = network;
        this._ap = network.accessPoints[0];

        this.actor = new St.BoxLayout({ style_class: 'nm-dialog-item',
                                        can_focus: true,
                                        reactive: true });
        this.actor.connect('key-focus-in', () => { this.emit('selected'); });
        let action = new Clutter.ClickAction();
        action.connect('clicked', () => { this.actor.grab_key_focus(); });
        this.actor.add_action(action);

        let title = this._ap._ssid;
        this._label = new St.Label({ text: title });

        this.actor.label_actor = this._label;
        this.actor.add(this._label, { x_align: St.Align.START });

        this._selectedIcon = new St.Icon({ style_class: 'nm-dialog-icon',
                                           icon_name: 'object-select-symbolic' });
        this.actor.add(this._selectedIcon);

        this._icons = new St.BoxLayout({ style_class: 'nm-dialog-icons' });
        this.actor.add(this._icons, { expand: true, x_fill: false, x_align: St.Align.END });

        this._secureIcon = new St.Icon({ style_class: 'nm-dialog-icon' });
        if (this._ap._secType != apsecurity.none)
            this._secureIcon.icon_name = 'network-wireless-encrypted-symbolic';
        this._icons.add_actor(this._secureIcon);

        this._signalIcon = new St.Icon({ style_class: 'nm-dialog-icon' });
        this._icons.add_actor(this._signalIcon);

        this._sync();
    },

    _sync() {
        this._signalIcon.icon_name = this._getSignalIcon();
    },

    updateBestAP(ap) {
        this._ap = ap;
        this._sync();
    },

    setActive(isActive) {
        this._selectedIcon.opacity = isActive ? 255 : 0;
    },

    _getSignalIcon() {
            return wifimenu.wireless_symbols[this._ap.strength];
    }
});
Signals.addSignalMethods(WirelessDialogItem.prototype);


var WifiSelectDialog = new Lang.Class({
    Name: 'WifiSelectDialog',
    Extends: ModalDialog.ModalDialog,

    _init: function (client, device, activessid) {
        this.parent({ styleClass: 'nm-dialog' });

        this._client = client;
        this._device = device;
        this.active_ssid = activessid;

        this._buildLayout();
        this._updateSensitivity();

        let id = Main.sessionMode.connect('updated', () => {
            if (Main.sessionMode.allowSettings)
                return;

            Main.sessionMode.disconnect(id);
            this.close();
        });

    },

    _buildLayout() {
        let headline = new St.BoxLayout({ style_class: 'nm-dialog-header-hbox' });

        let icon = new St.Icon({ style_class: 'nm-dialog-header-icon',
                                 icon_name: 'network-wireless-signal-excellent-symbolic' });

        let titleBox = new St.BoxLayout({ vertical: true });
        let title = new St.Label({ style_class: 'nm-dialog-header',
                                   text: _("Wi-Fi Networks") });
        let subtitle = new St.Label({ style_class: 'nm-dialog-subheader',
                                      text: _("Select a network") });
        titleBox.add(title);
        titleBox.add(subtitle);

        headline.add(icon);
        headline.add(titleBox);

        this.contentLayout.style_class = 'nm-dialog-content';
        this.contentLayout.add(headline);

        this._stack = new St.Widget({ layout_manager: new Clutter.BinLayout() });

        this._itemBox = new St.BoxLayout({ vertical: true });
        this._scrollView = new St.ScrollView({ style_class: 'nm-dialog-scroll-view' });
        this._scrollView.set_x_expand(true);
        this._scrollView.set_y_expand(true);
        this._scrollView.set_policy(Gtk.PolicyType.NEVER,
                                    Gtk.PolicyType.AUTOMATIC);
        this._scrollView.add_actor(this._itemBox);
        this._stack.add_child(this._scrollView);

        this.contentLayout.add(this._stack, { expand: true });

        this._disconnectButton = this.addButton({ action: this.close.bind(this),
                                                  label: _("Cancel"),
                                                  key: Clutter.Escape });
        this._connectButton = this.addButton({ action: this._connect.bind(this),
                                               label: _("Connect"),
                                               key: Clutter.Return });
    },
    _updateSensitivity() {
        let connectSensitive = this._selectedNetwork && (this._selectedNetwork != this._activeNetwork);
        this._connectButton.reactive = connectSensitive;
        this._connectButton.can_focus = connectSensitive;
    },
    _selectNetwork(network) {
        if (this._selectedNetwork)
            this._selectedNetwork.item.actor.remove_style_pseudo_class('selected');

        this._selectedNetwork = network;
        this._updateSensitivity();

        if (this._selectedNetwork)
            this._selectedNetwork.item.actor.add_style_pseudo_class('selected');
    },

    _createNetworkItem(network) {
        network.item = new WirelessDialogItem(network);
        network.item.setActive(network == this._selectedNetwork);
        network.item.connect('selected', () => {
            Util.ensureActorVisibleInScrollView(this._scrollView, network.item.actor);
            this._selectNetwork(network);
        });
        network.item.actor.connect('destroy', () => {
            let keyFocus = global.stage.key_focus;
            if (keyFocus && keyFocus.contains(network.item.actor))
                this._itemBox.grab_key_focus();
        });
    },
    
    _accessPointAdded(device, network) {
        let pos = -1;

        this._createNetworkItem(network);
        if (network.ssid == this.active_ssid) {
            pos = 0;
            this._activeNetwork = network;
            this._activeNetwork.item.setActive(true);
        }
        this._itemBox.insert_child_at_index(network.item.actor, pos);
    },
    destroy: function() {
        this.parent();
    },
    _connect: function() {
        if (this._selectedNetwork.key_mgmt == "unknown") {
            wifimenu.netctl_dbus.menu.connect_to_network(this._selectedNetwork, "");
        } else {
            this.passwordDialog = new WifiPasswordDialog(this._selectedNetwork);
            this.passwordDialog.open(global.get_current_time());
        }
        this.close();
    },
});


var WifiPasswordDialog = new Lang.Class({
    Name: 'WifiPasswordDialog',
    Extends: ModalDialog.ModalDialog,

    _init: function(network) {
        this.parent({ styleClass: 'prompt-dialog' });
        this._network = network;
        this.secret = {};
        this.secret.identity = "";
        this.secret.eap = "";
        this.secret.psk = "";
        this.secret.password = "";
        
        this.secret.cacert = "";
        this.secret.clientcert = "";
        this.secret.privkey = "";
        this.secret.privkeypass = "";

        let title = _("Enter the Wi-Fi credentials for “%s”").format(network.ssid);

        let dialogParams = { title: "Wi-Fi Credentials",
                             body: title};

        let contentBox = new Dialog.MessageDialogContent(dialogParams);
        this.contentLayout.add_actor(contentBox);

        let layout = new Clutter.GridLayout({ orientation: Clutter.Orientation.VERTICAL });
        
        let secretTable = new St.Widget({ style_class: 'network-dialog-secret-table',
                                          layout_manager: layout });
        layout.hookup_style(secretTable);

        if (network.key_mgmt == "wpa-psk") {
            let label = new St.Label({ style_class: 'prompt-dialog-password-label',
                                       text: "Password",
                                       x_align: Clutter.ActorAlign.START,
                                       y_align: Clutter.ActorAlign.CENTER });
            label.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;

            let entry = new St.Entry({ style_class: 'prompt-dialog-password-entry',
                                       text: '', can_focus: true,
                                       reactive: true,
                                       x_expand: true });

            ShellEntry.addContextMenu(entry, { isPassword: true });

            this.setInitialKeyFocus(entry);

            entry.clutter_text.connect('activate', Lang.bind(this, this._onOk));
            entry.clutter_text.connect('text-changed', Lang.bind(this, function() {
                this.secret.psk = entry.get_text();
                if (this.secret.validate)
                    this.secret.valid = this.secret.validate(secret);
                else
                    this.secret.valid = this.secret.psk.length > 0;
                this._updateOkButton();
            }))

            entry.clutter_text.set_password_char('\u25cf');
            
            layout.attach(label, 0, 0, 1, 1);
            layout.attach(entry, 1, 0, 1, 1);
        }
        else if (network.key_mgmt == "wpa-eap") {
            this.secret.eap = "MSCHAPV2"

            let identity = new St.Label({ style_class: 'prompt-dialog-password-label',
                                       text: "Identity",
                                       x_align: Clutter.ActorAlign.START,
                                       y_align: Clutter.ActorAlign.CENTER });
            identity.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;

            let identry = new St.Entry({ style_class: 'prompt-dialog-password-entry',
                                         text: '', can_focus: true,
                                         reactive: true,
                                         x_expand: true });

            layout.attach(identity, 0, 0, 1, 1);
            layout.attach(identry, 1, 0, 1, 1);

            identry.clutter_text.connect('text-changed', Lang.bind(this, function() {
                this.secret.identity = identry.get_text();
            }))

            let label = new St.Label({ style_class: 'prompt-dialog-password-label',
                                       text: "Password",
                                       x_align: Clutter.ActorAlign.START,
                                       y_align: Clutter.ActorAlign.CENTER });
            label.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;

            let entry = new St.Entry({ style_class: 'prompt-dialog-password-entry',
                                       text: '', can_focus: true,
                                       reactive: true,
                                       x_expand: true });

            ShellEntry.addContextMenu(entry, { isPassword: true });

            this.setInitialKeyFocus(identry);

            entry.clutter_text.connect('activate', Lang.bind(this, this._onOk));
            entry.clutter_text.connect('text-changed', Lang.bind(this, function() {
                this.secret.password = entry.get_text();
                if (this.secret.validate)
                    this.secret.valid = this.secret.validate(secret);
                else
                    this.secret.valid = this.secret.password.length > 0;
                this._updateOkButton();
            }))

            entry.clutter_text.set_password_char('\u25cf');
            layout.attach(label, 0, 1, 1, 1);
            layout.attach(entry, 1, 1, 1, 1);

            let caLabel = new St.Label({ style_class: 'prompt-dialog-password-label',
                                       text: "CA Cert",
                                       x_align: Clutter.ActorAlign.START,
                                       y_align: Clutter.ActorAlign.CENTER });
            label.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
            
            let caFileSelect = new St.Button({label:"Select File",
                                              style_class: 'modal-dialog-button button',
                                              reactive: true,
                                              can_focus: true,
                                              x_fill: false,
                                              y_fill: false,
                                              track_hover: true});
            caFileSelect.secret_element = "cacert"
            
            let clientLabel = new St.Label({ style_class: 'prompt-dialog-password-label',
                                             text: "Client Cert",
                                             x_align: Clutter.ActorAlign.START,
                                             y_align: Clutter.ActorAlign.CENTER });
            label.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
            
            let clientFileSelect = new St.Button({label:"Select File",
                                                  style_class: 'modal-dialog-button button',
                                                  reactive: true,
                                                  can_focus: true,
                                                  x_fill: false,
                                                  y_fill: false,
                                                  track_hover: true});
            clientFileSelect.secret_element = "clientcert";
            
            caFileSelect.connect('clicked', Lang.bind(this, this._clickSelectFile));
            clientFileSelect.connect('clicked', Lang.bind(this, this._clickSelectFile));
            
            layout.attach(caLabel, 0, 2, 1, 1);
            layout.attach(caFileSelect, 1, 2, 1, 1);

            layout.attach(clientLabel, 0, 3, 1, 1);
            layout.attach(clientFileSelect, 1, 3, 1, 1);

            /*
            // Leaving this code in as it will be useful when adding support for TTLS

            let keyPassLabel = new St.Label({ style_class: 'prompt-dialog-password-label',
                                       text: "Key Password",
                                       x_align: Clutter.ActorAlign.START,
                                       y_align: Clutter.ActorAlign.CENTER });
            label.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;

            let keyPassEntry = new St.Entry({ style_class: 'prompt-dialog-password-entry',
                                       text: '', can_focus: true,
                                       reactive: true,
                                       x_expand: true });
            ShellEntry.addContextMenu(keyPassEntry, { isPassword: true });

            keyPassEntry.clutter_text.set_password_char('\u25cf');
            layout.attach(keyPassLabel, 0, 4, 1, 1);
            layout.attach(keyPassEntry, 1, 4, 1, 1);

            let combo = new ActionComboBox();
            layout.attach(combo.actor, 1, 5, 1, 1);
           */
            
        }

        contentBox.messageBox.add(secretTable);

        this._okButton = { label:  _("Connect"),
                           action: Lang.bind(this, this._onOk),
                           default: true
                         };

        this.setButtons([{ label: _("Cancel"),
                           action: Lang.bind(this, this.cancel),
                           key:    Clutter.KEY_Escape,
                         },
                         this._okButton]);

        this._updateOkButton();
    },
    _selectAuth: function() {
        
    },
    _clickSelectFile: function(fsButton) {
        let buff = '';
        let handleResponse = Lang.bind(this, function(rsp) {
            let jsonret;
            try {
                jsonret = JSON.parse(rsp);
            } catch (err) {
                jsonret = {error: err.toString()};
            }
            if (!jsonret.error) {
                fsButton.set_label(jsonret.file.toString());
                this.secret[fsButton.secret_element] = jsonret.file.toString();
            }
            
        });
        let reader = new Spawn.SpawnReader();
	let pathFileSelect = GLib.build_pathv('/', [Me.path, 'fileSelect.js']);

        this.popModal();
        this._group.hide();
        reader.spawn(GLib.get_home_dir(), ['/usr/bin/gjs', pathFileSelect], Lang.bind (this, function (line) {
            if (line != null) {
                buff += String(line) + '\n';
            } else {
                if (buff.length > 0) {
                    handleResponse(buff);
                }
            }
            this._group.show();
            this.pushModal();
        }));
        
    },

    _updateOkButton: function() {
        let valid = true;
        this._okButton.button.reactive = valid;
        this._okButton.button.can_focus = valid;
    },
    _onOk: function() {

        // This is a dirty hack for now, until a proper prefs.js can be built
        // This would be easier if we could use Gtk UI elements in a modal dialog, but apparently we can't
        //
        // If only user name and password is provided then it will attempt to authenticate with MSCHAPV2
        // If certs are provided then it will assume that the password field is the private key password
        // and authenticate with TLS
        
        if (this.secret.cacert != "" && this.secret.clientcert != "") {
            this.secret.privkeypassword = this.secret.password;
            this.secret.password = "";
            this.secret.privkey = this.secret.clientcert;
            this.secret.eap = "TLS";
        }
        wifimenu.netctl_dbus.menu.connect_to_network(this._network, this.secret);
        this.close(global.get_current_time());
    },
    cancel: function() {
        this.close(global.get_current_time());
    },
});
